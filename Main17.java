import java.util.Scanner;

public class Main {
    public static int fact(int f){
        int factorial_result = 1; 
        for (int i = 1; i <= f; i++) {
            factorial_result *= i;
        }
        return factorial_result;
    }   
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in); // создаём объект класса Scanner
        int i = sc.nextInt();
        System.out.println(fact(i));
    }
}